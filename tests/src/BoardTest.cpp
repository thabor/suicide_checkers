#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "Board.h"

TEST_CASE("Board size set correctly","[BoardSize]")
{
	Board board ;
	
	SECTION( "Board size of 6")
	{
		board.setsize( 6) ;
		REQUIRE( board.getsize() == 6) ;
	}

	SECTION( "Board size of 10")
	{
		board.setsize( 10) ;
		REQUIRE( board.getsize() == 10) ;
	}

	SECTION ( "Board size of 1200")
	{
		board.setsize( 1200) ;
		REQUIRE( board.getsize() == 1200) ;
	}
}

TEST_CASE( "Correct pieces added to board", "[BoardPieces]")
{
	Board board ;
	SECTION( "Board size 6")
	{
		board.setsize( 6) ;
		board.setpieces() ;

		SECTION ( "Total number of pieces on board.")
		{
			REQUIRE( board.getpieces().size() == 6*2) ;
		}

		SECTION( "Number of player 0 pieces.")
		{
			REQUIRE( board.getplayerpieces( 0).size() == 6) ;
		}

		SECTION( "Number of player 1 pieces.")
		{
			REQUIRE( board.getplayerpieces( 1).size() == 6) ;
		}
	}

	SECTION( "Board size 10")
	{
		board.setsize( 10) ;
		board.setpieces() ;

		SECTION ( "Total number of pieces on board.")
		{
			REQUIRE( board.getpieces().size() == 20*2) ;
		}

		SECTION( "Number of player 0 pieces.")
		{
			REQUIRE( board.getplayerpieces( 0).size() == 20) ;
		}

		SECTION( "Number of player 1 pieces.")
		{
			REQUIRE( board.getplayerpieces( 1).size() == 20) ;
		}
	}

	SECTION( "Board size 12")
	{
		board.setsize( 12) ;
		board.setpieces() ;

		SECTION ( "Total number of pieces on board.")
		{
			REQUIRE( board.getpieces().size() == 30*2) ;
		}

		SECTION( "Number of player 0 pieces.")
		{
			REQUIRE( board.getplayerpieces( 0).size() == 30) ;
		}

		SECTION( "Number of player 1 pieces.")
		{
			REQUIRE( board.getplayerpieces( 1).size() == 30) ;
		}
	}
}

TEST_CASE( "Test piece retrieval from board", "[PieceRetrieval]")
{
	Board board ;
	board.setsize( 6) ;
	board.setpieces() ;

	SECTION( "Player 0 pieces.")
	{
		SECTION( "Boundary piece - first.")
		{
			REQUIRE( board.getpiece( 1) == std::make_pair( 0, 1)) ;
		}

		SECTION( "Boundary piece - last.")
		{
			REQUIRE( board.getpiece( 6) == std::make_pair( 0, 6)) ;
		}
		SECTION( "Some middle piece.")
		{
			REQUIRE( board.getpiece( 3) == std::make_pair( 0, 3)) ;
		}
	}
	
	SECTION( "Player 1 pieces.")
	{
		SECTION( "Boundary piece - first.")
		{
			REQUIRE( board.getpiece( 13) == std::make_pair( 1, 13)) ;
		}

		SECTION( "Boundary piece - last.")
		{
			REQUIRE( board.getpiece( 18) == std::make_pair( 1, 18)) ;
		}
		SECTION( "Some middle piece.")
		{
			REQUIRE( board.getpiece( 16) == std::make_pair( 1, 16)) ;
		}
	}
	SECTION( "Invalid pieces.")
	{
		SECTION( "Boundary piece - first.")
		{
			REQUIRE( board.getpiece( 7) == std::make_pair( -1, -1)) ;
		}

		SECTION( "Boundary piece - last.")
		{
			REQUIRE( board.getpiece( 12) == std::make_pair( -1, -1)) ;
		}

		SECTION( "Some midle piece.")
		{
			REQUIRE( board.getpiece( 9) == std::make_pair( -1, -1)) ;
		}
	}
}

TEST_CASE( "Test piece availability on board", "[PieceAvailability]")
{
	Board board ;
	board.setsize( 8) ;
	board.setpieces() ;
	SECTION( "Available edge pieces.")
	{
		REQUIRE( board.haspiece( 1)) ;
		REQUIRE( board.haspiece( 12)) ;
		REQUIRE( board.haspiece( 21)) ;
		REQUIRE( board.haspiece( 32)) ;
	}

	SECTION( "Available middle pieces.")
	{
		REQUIRE( board.haspiece( 7)) ;
		REQUIRE( board.haspiece( 28)) ;
	}

	SECTION( "Unavailable edge pieces.")
	{
		REQUIRE( !board.haspiece( 13)) ;
		REQUIRE( !board.haspiece( 20)) ;
	}

	SECTION( "Unavaiilable middle pieces.")
	{
		REQUIRE( !board.haspiece( 14)) ;
		REQUIRE( !board.haspiece( 17)) ;
	}
}

TEST_CASE( "Test removal of player piece from board.", "[PieceRemoval]")
{
	Board board ;
	board.setsize( 10) ;
	board.setpieces() ;
	SECTION( "Remove valid player 0 piece.")
	{
		REQUIRE( board.haspiece( std::make_pair( 0, 3))) ;
		board.removepiece( std::make_pair( 0, 3)) ;
		REQUIRE( !board.haspiece( std::make_pair( 0, 3))) ;
		REQUIRE( board.getplayerpieces( 0).size() == 19) ;
	}

	SECTION( "Remove invalid player piece.")
	{
		REQUIRE( board.getpieces().size() == 40) ;
		REQUIRE( board.getplayerpieces( 0).size() == 20) ;
		REQUIRE( board.getplayerpieces( 1).size() == 20) ;
		board.removepiece( std::make_pair( 0, 21)) ;
		board.removepiece( std::make_pair( 1, 30)) ;
		REQUIRE( board.getplayerpieces( 1).size() == 20) ;
		REQUIRE( board.getplayerpieces( 0).size() == 20) ;
		REQUIRE( board.getpieces().size() == 40) ;
	}

	SECTION( "Remove valid player 1 piece.")
	{
		REQUIRE( board.haspiece( std::make_pair( 1, 45))) ;
		board.removepiece( std::make_pair( 1, 45)) ;
		REQUIRE( !board.haspiece( std::make_pair( 1, 45))) ;
		REQUIRE( board.getplayerpieces( 1).size() == 19) ;
	}
}
/*
TEST_CASE( "Test valid moves.")
{
	Board board ;
	
	SECTION ( "Test for board of size 6")
	{
		board.setsize( 6) ;
		board.removepiece( {}
	}

	SECTION( "Test for board of size 8")
	{
		board.setsize( 8) ;
	}
}
*/
/*
TEST_CASE( "Test whether move results in capture of any opponent piece.")
{
	Board board ;
	SECTION( "Board size 12")
	{
		board.setsize( 12) ;
		board.setpieces() ;


		SECTION( "Player 0 non-capture one forward move.")
		{
			REQUIRE( !board.capturemove( 0, 33, 39)) ;
		}

		SECTION( "Player 1 non-capture one forward move")
		{
			REQUIRE( !board.capturemove( 1, 44, 38)) ;
		}
		SECTION( "Player 0 capture move.")
		{
			board.removepiece( std::make_pair( 1, 52)) ;
			REQUIRE( board.capturemove( 0, 39, 52)) ;
		}

		SECTION( "Player 0 jump non-capture move.")
		{
			REQUIRE( !board.capturemove( 0, 29, 40)) ;
		}

		SECTION( "Player 1 capture move.")
		{
			board.removepiece( std::make_pair( 0, 21)) ;
			REQUIRE( board.capturemove( 1, 32, 21)) ;
		}

		SECTION( "Player 1 jump non-capture move.")
		{
			REQUIRE( !board.capturemove( 1, 44, 33)) ;
		}
	}

	SECTION( "Board size 6")
	{
		board.setsize( 6) ;
		board.setpieces() ;

		SECTION( "Player 0 non-capture one forward move.")
		{
			REQUIRE( !board.capturemove( 0, 6, 8)) ;
		}

		SECTION( "Player 1 non-capture one forward move")
		{
			REQUIRE( !board.capturemove( 1, 14, 11)) ;
		}
		SECTION( "Player 0 capture move.")
		{
			board.removepiece( std::make_pair( 1, 18)) ;
			REQUIRE( board.capturemove( 0, 11, 18)) ;
		}

		SECTION( "Player 0 jump non-capture move.")
		{
			REQUIRE( !board.capturemove( 0, 5, 12)) ;
		}

		SECTION( "Player 1 capture move.")
		{
			board.removepiece( std::make_pair( 0, 2)) ;
			REQUIRE( board.capturemove( 1, 7, 2)) ;
		}

		SECTION( "Player 1 jump non-capture move.")
		{
			REQUIRE( !board.capturemove( 1, 14, 9)) ;
		}

	}
}
*/
TEST_CASE( "Test whether the move made is a jump")
{
	Board board ;
	board.setsize( 12) ;
	
	SECTION( "Single non-jump moves.")
	{
		REQUIRE( !board.jump( 16, 11)) ;
		REQUIRE( !board.jump( 8, 1)) ;
		REQUIRE( !board.jump( 61, 55)) ;
		REQUIRE( !board.jump( 42, 48)) ;
		REQUIRE( !board.jump( 37, 33)) ;
	}

	SECTION( "Double or more jumps.")
	{
		REQUIRE( board.jump( 2, 22)) ;
		REQUIRE( board.jump( 21, 32)) ;
		REQUIRE( board.jump( 65, 33)) ;
		REQUIRE( board.jump( 16, 36)) ;
	}
}

/*
TEST_CASE( "Test validity of player move.","MoveValidation")
{
	Board board ;
	//use board big enough for all possible cases
	board.setsize( 12) ;
	board.setpieces() ;
	
	SECTION( "Test move back restricted.")
	{
		SECTION( "Player 0 move back.")
		{
			REQUIRE( !board.movevalid( 0, 22, 15)) ;
			REQUIRE( !board.movevalid( 0, 14, 9)) ;
		}

		SECTION( "Player 1 move back.")
		{
			REQUIRE( !board.movevalid( 1, 58, 64)) ;
			REQUIRE( !board.movevalid( 1, 32, 38)) ;
		}
	}

	SECTION( "Test off-board moves restricted.")
	{
		SECTION( "Player 0 move forward off board.")
		{
			REQUIRE( !board.movevalid( 0, 72, 73)) ;
		}

		SECTION( "Player 1 move forward off board.")
		{
			REQUIRE( !board.movevalid( 1, 4, -3)) ;
		}
	}

	SECTION( "Test move to occupied cells restriced.")
	{
		SECTION( "Player 0 moves to cell occupied by its piece.")
		{
			REQUIRE( !board.movevalid( 0, 9, 15)) ;
		}

		SECTION( "Player 0 moves to cell occupied by opponents piece.")
		{
			REQUIRE( !board.movevalid( 0, 30, 47)) ;
		}


		SECTION( "Player 1 moves to cell occupied by its piece.")
		{
			REQUIRE( !board.movevalid( 1, 72, 65)) ;
		}

		SECTION( "Player 1 moves to cell occupied by opponents piece.")
		{
			REQUIRE( !board.movevalid( 1, 34, 27)) ;
		}
	}

	SECTION( "Test double move with no capture resticted.")
	{
		SECTION( "Player 0 double move with no capture.")
		{
			REQUIRE( !board.movevalid( 0, 26, 39)) ;
		}

		SECTION( "Player 1 double move no capture.")
		{
			REQUIRE( !board.movevalid( 1, 47, 34)) ;
		}
	}
}
*/
