CC = g++

INCLUDES = -I./main/headers -I./tests/libheaders/

SOURCES = ./main/src/*.cpp

TARGET = ./bin/main.o
.PHONY: depend clean

main: $(TARGET)
	@echo Main program compiled
$(TARGET): clean
	$(CC) -o $(TARGET) $(SOURCES) $(INCLUDES)
clean:
	$(RM) *~ $(TARGET)

depend: $(SOURCES)
	makedepend $(INCLUDES) $^

# DO NOT DELETE THIS LINE -- make depend needs it
