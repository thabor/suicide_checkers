#include "RandAlg.h"
#include <random>

void RandAlg::updateboard( std::vector< std::pair< int, int>> pieces)
{
	board.setpieces( pieces) ;
}

void RandAlg::updateboard( int boardsize)
{
	board.setsize( boardsize) ;
	board.setpieces() ;
}

void RandAlg::setplayernumber( int pn)
{
	playernumber = pn ;
}

int RandAlg::getplayernumber() const
{
	return playernumber ;
}

std::pair< int, int> RandAlg::makemove() const
{
	std::vector< std::vector< std::pair< int, int>>> availablemoves = board.getvalidmoves( playernumber) ;
	//if compulsory moves aren't empty then return that
	if( availablemoves[1].size() > 0 )
	{
		return availablemoves[1][ random() % availablemoves[1].size()] ;
	}
	else if( availablemoves[0].size() >0)
	{
		return availablemoves[0][ random() % availablemoves[0].size()] ;
	}
	else
	{
		return std::make_pair( -1, -1) ;
	}
}
