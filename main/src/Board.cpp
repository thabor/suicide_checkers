#include "Board.h"

void Board::setsize( int s)
{
	size = s ;
}

int Board::getsize() const
{
	return size ;
}

void Board::setpieces()
{
	int bs = getsize() ;
	int playernumofpieces = bs/2 * ( bs/2 - 1) ;
	
	pieces.clear() ;

	for( int i = 1 ; i <= playernumofpieces; ++i)
	{
		//add top player piece
		pieces.push_back( std::make_pair( 0, i)) ;
		//add bottom player piece
		pieces.push_back( std::make_pair( 1, bs*bs/2 -i+1)) ;
	}
}

void Board::setpieces( std::vector< std::pair< int, int>> pcs)
{
	pieces = pcs ;
}

std::vector< std::pair< int, int>> Board::getpieces() const
{
	return pieces ;
}

void Board::updatepiece( std::pair< int, int> piece, std::pair< int, int> updatedpiece)
{
	//run through all the board pieces and change the given one like specified
	for( int i = 0; i < pieces.size(); ++i)
	{
		if ( pieces[i] == piece)
		{
			pieces[i] = updatedpiece ;
			//we've found the piece we were looking for so we don't have to search through the vector anymore
			break ;
		}
	}
}

std::vector< int> Board::getplayerpieces( int player) const
{
	std::vector< int> playerpieces ;
	for ( int i = 0; i < pieces.size(); ++i)
	{
		if( pieces[i].first == player)
			playerpieces.push_back( pieces[i].second) ;
	}

	return playerpieces ;
}

std::pair< int, int> Board::getpiece( int post) const
{
	//search for piece
	for( int i = 0; i < pieces.size() ; ++i)
	{
		if( pieces[i].second == post)
		{
			return pieces[i] ;
		}
	}
	return std::make_pair( -1, -1) ;
}

bool Board::haspiece( int post) const
{
	//no piece found
	if( getpiece( post) == std::make_pair( -1, -1))
	{
		return false ;
	}
	return true ;
}

bool Board::haspiece( std::pair< int, int> piece) const
{
	//piece found
	if( getpiece( piece.second) == piece)
	{
		return true ;
	}

	return false ;
}

void Board::removepiece( std::pair< int, int> piece)
{
	//search for piece and if found remove it
	for( int i = 0; i < getpieces().size(); ++i)
	{
		//if found, erase and stop searching
		if( pieces[i] == piece)
		{
			pieces.erase( pieces.begin() + i) ;
			break ;
		}
	}
}

std::vector< std::vector<int>> Board::getvalidmoves( std::pair< int, int> piece) const
{
	std::vector< int> validsinglemoves, validcapturemoves ;
	//for top player
	if( piece.first == 0)
	{
		//cannot move, end of board
		if( piece.second > getsize()/2 * ( getsize() - 1))
		{
			//empty vectors
			return {{},{}} ;
		}
		//on far right of the board
		else if( piece.second % getsize() == getsize()/2 )
		{
			//only return left valid moves
			if( !haspiece( piece.second + getsize()/2))
			{
				//immediate cell to our left unoccupied, can make single move but can't make double cause there's no capture
				return {{ piece.second + getsize()/2}, {}} ;
			}
			else
			{
				//cell to our left occupied
				if ( haspiece( { 1, piece.second + getsize()/2}) && ( piece.second < getsize()/2 * ( getsize() - 2)) && !haspiece( piece.second + getsize() - 1))
				{
					//cell to our left occupied by opponent and cell to their bottom left empty, we can capture
					return {{}, { piece.second + getsize() - 1}} ;
				}
				return {{},{}} ;
			}
		}
		//on far left of the board
		else if( (piece.second - 1) % getsize() == getsize()/2)
		{
			//only return right valid moves
			if( !haspiece( piece.second + getsize()/2))
			{
				return {{ piece.second + getsize()/2}, {}} ;
			}
			else
			{
				if( haspiece( { 1, piece.second + getsize()/2}) && !haspiece( piece.second + getsize() + 1))
				{
					return {{},{ piece.second + getsize() + 1}} ;
				}
				return {{},{}} ;
			}
		}
		//on the far right, left adjusted side of the board
		else if ( piece.second % getsize() == 0 )
		{
			//can only make single right move and single or double left
			if( !haspiece( piece.second + getsize()/2))
			{
				validsinglemoves.push_back( piece.second + getsize()/2) ;
			}
			
			if( !haspiece( piece.second + getsize()/2 - 1))
			{
				validsinglemoves.push_back( piece.second + getsize()/2 - 1);
			}
			else if( haspiece( { 1, piece.second + getsize()/2 - 1}) && !haspiece( piece.second + getsize() - 1))
			{
				validcapturemoves.push_back( piece.second + getsize() - 1) ;
			}
			
			return { validsinglemoves, validcapturemoves} ;
		}
		//in the far left, right adjusted side
		else if( ( piece.second - 1) % getsize() == 0)
		{
			//can only make single left and single or double right
			if( !haspiece( piece.second + getsize()/2))
			{
				validsinglemoves.push_back( piece.second + getsize()/2) ;
			}

			if( !haspiece( piece.second + getsize()/2 + 1))
			{
				validsinglemoves.push_back( piece.second + getsize()/2 + 1);
			}
			else if( haspiece( { 1, piece.second + getsize()/2 + 1}) && ( piece.second < getsize()/2 * ( getsize() - 2)) && !haspiece( piece.second + getsize() + 1))
			{
				validcapturemoves.push_back( piece.second + getsize() + 1) ;
			}
			return { validsinglemoves, validcapturemoves} ;
		}
		// somewhere else that's right adjusted
		else if ( piece.second % getsize() < getsize()/2)
		{
			//single left
			if( !haspiece( piece.second + getsize()/2))
			{
				validsinglemoves.push_back( piece.second + getsize()/2) ;
			}
			//double left
			else if ( haspiece( { 1, piece.second + getsize()/2}) && ( piece.second < getsize()/2 * ( getsize() - 2)) && !haspiece( piece.second + getsize() - 1))
			{
				validcapturemoves.push_back( piece.second + getsize() - 1) ;
			}
			//single right
			if ( !haspiece( piece.second + getsize()/2 + 1))
			{
				validsinglemoves.push_back( piece.second + getsize()/2 + 1) ;
			}
			//double right
			else if ( haspiece( { 1, piece.second + getsize()/2 + 1}) && ( piece.second < getsize() * ( getsize() -2)) && !haspiece( piece.second + getsize() + 1))
			{
				validcapturemoves.push_back( piece.second + getsize() + 1) ;
			}
			return { validsinglemoves, validcapturemoves} ;
		}
		//somewhere in the middle that's left adjusted
		else
		{
			//single left
			if( !haspiece( piece.second + getsize()/2 - 1))
			{
				validsinglemoves.push_back( piece.second + getsize()/2 - 1) ;
			}
			//double left
			else if( haspiece( { 1, piece.second + getsize()/2 -1}) && !haspiece( piece.second + getsize() - 1))
			{
				validcapturemoves.push_back( piece.second + getsize() -1) ;
			}

			//single right
			if( !haspiece( piece.second + getsize()/2))
			{
				validsinglemoves.push_back( piece.second + getsize()/2) ;
			}
			else if( haspiece( { 1, piece.second + getsize()/2}) && !haspiece( piece.second + getsize() + 1))
			{
				validcapturemoves.push_back( piece.second + getsize() + 1) ;
			}
			return { validsinglemoves, validcapturemoves} ;

		}
	}
	else
	{
		//player from bottom
		//cannot move, end of board
		if( piece.second <= getsize()/2)
		{
			//empty vectors
			return {{},{}} ;
		}
		//on far right of the board
		else if( piece.second % getsize() == getsize()/2 )
		{
			//only return left valid moves
			if( !haspiece( piece.second - getsize()/2))
			{
				//immediate cell to our left unoccupied, can make single move but can't make double cause there's no capture
				return {{ piece.second - getsize()/2}, {}} ;
			}
			else
			{
				//cell to our left occupied
				if ( haspiece( { 0, piece.second - getsize()/2}) && !haspiece( piece.second - getsize() - 1))
				{
					//cell to our left occupied by opponent and cell to their bottom left empty, we can capture
					return {{}, { piece.second - getsize() - 1}} ;
				}
				return {{},{}} ;
			}
		}
		//on far left of the board
		else if( (piece.second - 1) % getsize() == getsize()/2)
		{
			//only return right valid moves
			if( !haspiece( piece.second - getsize()/2))
			{
				return {{ piece.second - getsize()/2}, {}} ;
			}
			else
			{
				if( haspiece( { 0, piece.second - getsize()/2}) && ( piece.second > getsize()) && !haspiece( piece.second - getsize() + 1))
				{
					return {{},{ piece.second - getsize() + 1}} ;
				}
				return {{},{}} ;
			}
		}
		//on the far right, left adjusted side of the board
		else if ( piece.second % getsize() == 0 )
		{
			//can only make single right move and single or double left
			if( !haspiece( piece.second - getsize()/2))
			{
				validsinglemoves.push_back( piece.second - getsize()/2) ;
			}
			
			if( !haspiece( piece.second - getsize()/2 - 1))
			{
				validsinglemoves.push_back( piece.second - getsize()/2 - 1);
			}
			else if( haspiece( { 0, piece.second - getsize()/2 - 1}) && (piece.second > getsize()) && !haspiece( piece.second - getsize() - 1))
			{
				validcapturemoves.push_back( piece.second - getsize() - 1) ;
			}
			
			return { validsinglemoves, validcapturemoves} ;
		}
		//in the far left, right adjusted side
		else if( ( piece.second - 1) % getsize() == 0)
		{
			//can only make single left and single or double right
			if( !haspiece( piece.second - getsize()/2))
			{
				validsinglemoves.push_back( piece.second - getsize()/2) ;
			}

			if( !haspiece( piece.second - getsize()/2 + 1))
			{
				validsinglemoves.push_back( piece.second - getsize()/2 + 1);
			}
			else if( haspiece( { 0, piece.second - getsize()/2 + 1}) && !haspiece( piece.second - getsize() + 1))
			{
				validcapturemoves.push_back( piece.second - getsize() + 1) ;
			}
			return { validsinglemoves, validcapturemoves} ;
		}
		// somewhere else that's right adjusted
		else if ( piece.second % getsize() < getsize()/2)
		{
			//single left
			if( !haspiece( piece.second - getsize()/2))
			{
				validsinglemoves.push_back( piece.second - getsize()/2) ;
			}
			//double left
			else if ( haspiece( { 0, piece.second - getsize()/2}) && !haspiece( piece.second - getsize() - 1))
			{
				validcapturemoves.push_back( piece.second - getsize() - 1) ;
			}
			//single right
			if ( !haspiece( piece.second - getsize()/2 + 1))
			{
				validsinglemoves.push_back( piece.second - getsize()/2 + 1) ;
			}
			//double right
			else if ( haspiece( { 0, piece.second - getsize()/2 + 1}) && !haspiece( piece.second - getsize() + 1))
			{
				validcapturemoves.push_back( piece.second - getsize() + 1) ;
			}
			return { validsinglemoves, validcapturemoves} ;
		}
		//somewhere in the middle that's left adjusted
		else
		{
			//single left
			if( !haspiece( piece.second - getsize()/2 - 1))
			{
				validsinglemoves.push_back( piece.second - getsize()/2 - 1) ;
			}
			//double left
			else if( haspiece( { 0, piece.second - getsize()/2 - 1}) && ( piece.second > getsize())&& !haspiece( piece.second - getsize() - 1))
			{
				validcapturemoves.push_back( piece.second - getsize() - 1) ;
			}

			//single right
			if( !haspiece( piece.second - getsize()/2))
			{
				validsinglemoves.push_back( piece.second - getsize()/2) ;
			}
			else if( haspiece( { 0, piece.second - getsize()/2}) && ( piece.second > getsize()) && !haspiece( piece.second - getsize() + 1))
			{
				validcapturemoves.push_back( piece.second - getsize() + 1) ;
			}
			return { validsinglemoves, validcapturemoves} ;

		}
	}
}

std::vector< std::vector< std::pair< int, int>>> Board::getvalidmoves( int player) const
{
	//the first vector contains optional single moves and the second has compulsory capture moves
	std::vector< std::vector< std::pair< int, int>>> allvalidmoves = {{},{}} ;
	std::vector< int> playerpieces = getplayerpieces( player) ;

	//go through each piece of the current player
	for ( int piece : playerpieces)
	{
		//get the moves that that piece can make
		std::vector< std::vector< int>> piecemoves = getvalidmoves( { player, piece}) ;

		//add single moves to optional move list in the form { position, destination}
		for( int move : piecemoves[0])
		{
			allvalidmoves[0].push_back( { piece, move}) ;
		}

		//do the same as above for capture moves
		for ( int move : piecemoves[1])
		{
			allvalidmoves[1].push_back( { piece, move}) ;
		}
	}

	return allvalidmoves ;
}

/* New approach, instead of testing for move validity, reduce the work by providing set of valid moves as an option
bool Board::movevalid( int player, int post, int dest) const
{
	if( player == 0)
	{
		//player 0 should move down the board to higher destination values. Also, no move isn't a valid move
		if( dest <= post)
		{
			return false ;
		}
		//player 0 should not move to a destination greater than number of total cells on board
		else if( dest > getsize() * getsize()/2)
		{
			return false ;
		}
		//cannot move to occupied cells
		else if( haspiece( dest))
		{
			return false ;
		}
	}
	else
	{
		//player 1 should move up the board to lower destination values. Also, staying in one place is not valid.
		if( dest >= post)
		{
			return false ;
		}
		//player 1 should not move to a destination values beyond the first cell
		else if( dest < 1)
		{
			return false ;
		}
		//cannot move to occupied cells
		else if( haspiece( dest))
		{
			return false ;
		}
	}

	return true ;
}
*/
/*
bool Board::capturemove( int player, int post, int dest) const
{
	//if not jump, it won't be a capture
	if( !jump( post, dest))
	{
		return false ;
	}
	//the thing below is an enumeration, it is similar to saying 'int CAPTURE_RIGHT=0' ; 'int CAPTURE_LEFT=1' ...
	enum { CAPTURE_RIGHT, CAPTURE_LEFT, DEST_RIGHT} ;

	//the array below will contain the position we jump over if we jump right, jump left, and our respective destinations from the given position ( post)
	std::vector< int> utilitypieces ;
	if( player == 0)
	{
	       utilitypieces = { post + getsize()/2, post + getsize()/2 + 1, post + getsize() - 1} ;
	       //check cell player jumps to
	       if( dest == utilitypieces[DEST_RIGHT])
	       {
		       //piece on pieces[CAPTURE_RIGHT] should be opponent's for us to return true
			return getpiece( utilitypieces[CAPTURE_RIGHT]).first == 1 ;
	       }
	       else
	       {
		       //jumping left
		       return getpiece( utilitypieces[CAPTURE_LEFT]).first == 1 ;
	       }
	}
	else
	{
		utilitypieces = { post - getsize()/2, post - getsize()/2 - 1, post - getsize() + 1} ;
		if( dest == utilitypieces[DEST_RIGHT])
		{
			return getpiece( utilitypieces[CAPTURE_RIGHT]).first == 0 ;
		}
		else
		{
			return getpiece( utilitypieces[CAPTURE_LEFT]).first == 0 ;
		}
	}
}
*/
bool Board::jump( int post, int dest) const
{
	//if one forward move, its not a jump, else it is
	if( -getsize()/2 - 1 <= dest - post && dest - post <= getsize()/2 + 1)
	{
		return false ;
	}
	return true ;
}
