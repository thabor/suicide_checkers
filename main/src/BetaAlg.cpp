#include "BetaAlg.h"
#include <random>

void BetaAlg::setplayernumber( int pn)
{
	playernumber = pn ;
}

int BetaAlg::getplayernumber() const
{
	return playernumber ;
}

void BetaAlg::updateboard( std::vector< std::pair< int, int>> pieces)
{
	board.setpieces( pieces) ;
}

void BetaAlg::updateboard( int boardsize)
{
	board.setsize( boardsize) ;
	board.setpieces() ;
}

std::pair< int, int> BetaAlg::makemove() const
{
	//board the algorithm uses to test stuff
	Board memoryboard ;
	memoryboard.setsize( board.getsize()) ;
	memoryboard.setpieces( board.getpieces()) ;
	//looks through the list of available moves and makes random capture/compulsory move that will rersult in less compulsory moves else makes a single move that increases opponent's compulsory moves by moving closer to opponent
	std::vector< std::vector< std::pair< int, int>>> allvalidmoves = memoryboard.getvalidmoves( getplayernumber()) ;
	//make compulsory move
	if ( allvalidmoves[1].size() > 0)
	{
		//we should make a compulsory move that does a single capture, will always increase opponent's compulsory move if we can do double
		return allvalidmoves[1][ random() % allvalidmoves[1].size()] ;
	}
	//make single move since compulsory move not available
	else if( allvalidmoves[0].size() > 0)
	{
		int opponent = 0 ;
		if( getplayernumber() == 0 )
		{
			opponent = 1;
		}
		int opponentscapturemoves = memoryboard.getvalidmoves( opponent)[1].size() ;
	
		//make single move that increases opponent's capture moves
		for( std::pair< int, int> move : allvalidmoves[0])
		{
			//check if making this move will increase opponent's capture moves
			//falsely play this move so to check its effect
			memoryboard.updatepiece( { getplayernumber(), move.first} , { getplayernumber(), move.second}) ;
			if ( board.getvalidmoves( opponent)[1].size() > opponentscapturemoves )
			{
				//this move increases player's capture moves, play it
				return move ;
			}
			//this move doesn't increase opponent's capture moves, undo it and look for other moves
			memoryboard.updatepiece( { getplayernumber(), move.second} , { getplayernumber(), move.first}) ;
		}
		//no move played increases opponent's capture moves so just play any random move
		return allvalidmoves[0][ random() % allvalidmoves[0].size()] ;
	}
	//no move can be made
	else
	{
		return std::make_pair( -1, -1) ;
	}
}
