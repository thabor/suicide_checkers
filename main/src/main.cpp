#include "Board.h"
#include "RandAlg.h"
#include "BetaAlg.h"

#include <fstream>
#include <vector>
#include <tuple>

#include <iostream>

//reads in board sizes and returns them in a vector
std::vector< int> readinboards() ;
//plays game and logs to output file
void playgame( BetaAlg& player1, RandAlg& player2, Board& board, std::ofstream& outputfile) ;

int main()
{
	Board board ;
	std::vector< int> boardsizes = readinboards() ;

	std::ofstream outputfile( "results.txt") ;

	//init players
	BetaAlg player0 ;
	RandAlg player1 ;
	player0.setplayernumber( 0) ;
	player1.setplayernumber( 1) ;

	//for each board size, play game
	for( int boardsize : boardsizes)
	{
		//set board for play
		board.setsize( boardsize) ;
		board.setpieces() ;

		//update player's knowledge of boards
		player0.updateboard( boardsize) ;
		player1.updateboard( boardsize) ;

		//get players' move
		outputfile << boardsize << "\n" ;

		playgame( player0, player1, board, outputfile) ;
		
		outputfile << "\n" ;
	}
	outputfile.close() ;
	return 0 ;
}

std::vector< int> readinboards()
{
	std::ifstream inputfile( "input.txt") ;
	std::string size ;
	std::vector< int> sizes ;
	
	while( std::getline( inputfile, size))
	{
		sizes.push_back( std::stoi( size)) ;
	}

	inputfile.close() ;
	return sizes ;
}

void playgame( BetaAlg& player1, RandAlg& player2, Board& board, std::ofstream& outputfile)
{
	bool forfeit1 = false , forfeit2 = false ;
	while( !( forfeit1 && forfeit2) )
	{
		//play player 1
		player1.updateboard( board.getpieces()) ;
		std::pair< int, int> move = player1.makemove();

		if( move == std::make_pair( -1, -1))
		{
			//player 1 can't move
			forfeit1 = true ;
		}
		else 
		{
			if( board.jump( move.first, move.second))
			{
				//log appropriately and remove opponent's piece
				int jumped = -1 ;
				if( move.first % board.getsize() > 0 && (move.first % board.getsize() <= board.getsize()/2))
				{
					//right adjusted cell
					//jumped to left
					jumped = move.first + board.getsize()/2 ;
					if( move.first + board.getsize() < move.second)
					{
					//jumped right
						jumped += 1 ;
					}
				}
				else
				{
					//left adjusted cell
					jumped = move.first + board.getsize()/2 - 1 ;
					if( move.first + board.getsize() < move.second)
					{
						jumped += 1 ;
					}
				}
				//log
				outputfile << "p1 " << move.first << "x" << move.second << "(" << jumped << ")\n" ;
				//remove opponent's piece
				board.removepiece( { player2.getplayernumber(), jumped}) ;
			}
			else
			{
				outputfile << "p1 " << move.first << "-" << move.second <<  "\n" ;
			}
			board.updatepiece( { player1.getplayernumber(), move.first}, { player1.getplayernumber(), move.second}) ;
			forfeit1 = false ;
		}

		//play player 2
		player2.updateboard( board.getpieces()) ;
		move = player2.makemove() ;
		if( move == std::make_pair( -1, -1))
		{
			//player 2 forfeits move
			forfeit2 = true ;
		}
		else
		{
			if( board.jump( move.first, move.second))
			{
				
				//log appropriately and remove opponent's piece
				int jumped = -1 ;
				if( move.first % board.getsize() > 0 && (move.first % board.getsize() <= board.getsize()/2))
				{
					//right adjusted cell
					//jumped to left
					jumped = move.first - board.getsize()/2 ;
					if( move.first - board.getsize() < move.second)
					{
					//jumped right
						jumped += 1 ;
					}
				}
				else
				{
					//left adjusted cell
					jumped = move.first - board.getsize()/2 - 1 ;
					if( move.first - board.getsize() < move.second)
					{
						jumped += 1 ;
					}
				}
				//log
				outputfile << "p2 " << move.first << "x" << move.second << "(" << jumped << ")\n" ;
				//remove opponent's piece
				board.removepiece( { player1.getplayernumber(), jumped}) ;
			}
			else
			{
				outputfile << "p2 " << move.first << "-" << move.second << "\n" ;
			}
			board.updatepiece( { player2.getplayernumber(), move.first}, { player2.getplayernumber(), move.second}) ;
			forfeit1 = false ;

		}
	}

	//report winner and number of pieces left
	int tp1 = board.getplayerpieces( player1.getplayernumber()).size() ;
	int tp2 = board.getplayerpieces( player2.getplayernumber()).size() ;
	outputfile << "tp1 " << tp1 << "\n" ;
	outputfile << "tp2 " << tp2 << "\n" ;

	if( tp1 < tp2 )
	{
		outputfile << "wp1\n" ;
	}
	else if ( tp1 > tp2 )
	{
		outputfile << "wp2\n" ;
	}
	else
	{
		outputfile << "d\n" ;
	}

}
