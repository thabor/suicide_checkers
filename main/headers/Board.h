#ifndef BOARD_H
#define BOARD_H
#include <vector>
#include <tuple>

class Board
{
	public :
		/*Adds an association ( Player, Position) inside a vector to represent the initial game state.
		 * Player= 0( player from top) or 1( player frm bottom of board).
		 * Position is a number from 1 to n*n where n is the board size. This denotes the position of a player's piece on the board
		 * e.g ( 1, 17) denotes the bottom player's piece at position 17 on the board
		 */
		void setpieces() ;
		void setpieces( std::vector< std::pair< int, int>> pcs) ;
		std::vector< std::pair< int, int>> getpieces() const ;
		void setsize( int bs) ;
		int getsize() const ;
		//updates the board by changing the given piece to one with a different position
		void updatepiece( std::pair< int, int> piece, std::pair< int, int> updatedpiece) ;
		//returns positions of all pieces of given player
		std::vector< int> getplayerpieces( int player) const ;
		//returns the piece at the specified position along with the player to whom it belongs if it exists else it returns the sentinel pair( -1,-1)
		std::pair< int, int> getpiece( int post) const ;
		//reports whether the board contains a piece at given position
		bool haspiece( int post) const ;
		//reports whether the board has a piece owned by some specified player at given position
		bool haspiece( std::pair< int, int> piece) const ;
		//removes the given piece from board if its available
		void removepiece( std::pair< int, int> piece) ;
		//given the player piece, return a list of valid destinations with the first being single moves and the second being capture moves
		std::vector< std::vector< int>> getvalidmoves( std::pair< int, int> piece) const ;
		//given a player, get list of all available moves in the format (post,dest)
		std::vector< std::vector< std::pair< int, int>>> getvalidmoves( int player) const ;
		//given the player, will the move made result in the capture of an opponent piece
		//bool capturemove( int player, int post, int destination) const ;
		//checks the cell span of the given move
		bool jump( int post, int dest) const ;
	private :
		int size ;
		std::vector< std::pair< int, int>> pieces ;
};
#endif
