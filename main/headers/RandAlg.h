#ifndef _RAND_ALG_H
#define _RAND_ALG_H
#include "Board.h"
#include <vector>
#include <tuple>

class RandAlg
{
	public :
		void setplayernumber( int pn) ;
		int getplayernumber() const ;
		//updates the player's view of the board
		void updateboard( std::vector< std::pair< int,int>> pieces) ;
		void updateboard( int boardsize) ;
		//returns a move ( position, destination) randomly
		std::pair< int, int> makemove() const ;
	private :
		Board board ;
		int playernumber ;
};

#endif
