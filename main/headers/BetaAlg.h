#ifndef _BETA_ALG_H
#define _BETA_ALG_H
#include "Board.h"
#include <vector>
#include <tuple>

class BetaAlg
{
	public :
		void setplayernumber( int pn) ;
		int getplayernumber() const ;
		void updateboard( std::vector< std::pair< int, int>> pieces) ;
		void updateboard( int boardsize)  ;
		std::pair< int, int> makemove() const ;
	private :
		int playernumber ;
		Board board ;
};

#endif
